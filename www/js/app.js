angular.module('scienteer', ['ionic',
  'scienteer.AuthController',
  'scienteer.MessagingController',
  'scienteer.ProjectController',
  'scienteer.MainController',
  'scienteer.AuthService',
  'scienteer.MessagingService',
  'scienteer.ProjectService',
  'scienteer.MainService',
  'ngSanitize'])

  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      $ionicPlatform.ready(function() {

        if(localStorage.getItem("push_notification") == null && localStorage.getItem("push_notification") == "") {
          FCMPlugin.getToken(
            function(token){
              localStorage.setItem("push_notification", token);
              console.log('Setting push notification token to: ' + token);
            },
            function(err){
              console.log('error retrieving token: ' + err);
            }
          )
        }

        FCMPlugin.onNotification(
          function(data){
            if(data.wasTapped){
              //Notification was received on device tray and tapped by the user.
              alert( JSON.stringify(data) );
            }else{
              //Notification was received in foreground. Maybe the user needs to be notified.
              alert( JSON.stringify(data) );
            }
          },
          function(msg){
            console.log('onNotification callback successfully registered: ' + msg);
          },
          function(err){
            console.log('Error registering onNotification callback: ' + err);
          }
        );
      });


    });
  })

  .filter('hrefToJS', function ($sce, $sanitize) {
    return function (text) {
      var regex = /href="([\S]+)"/g;
      var newString = $sanitize(text).replace(regex, "onClick=\"window.open('$1', '_system', 'location=yes')\"");
      return $sce.trustAsHtml(newString);
    }
  })

  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.scrolling.jsScrolling(false);

    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'MainCtrl'
      })

      .state('app.main', {
        url: '/main',
        views: {
          'menuContent': {
            templateUrl: 'templates/main.html',
            controller: 'MainMenuCtrl'
          }
        }
      })

      .state('app.messages', {
          url: '/messages',
          views: {
              'menuContent': {
                  templateUrl: 'templates/messages/messages.html',
                  controller: 'MessagesCtrl'
              }
          }
      })

      .state('app.messages-view', {
        url: '/messages/view/:messageId',
        views: {
          'menuContent': {
            templateUrl: 'templates/messages/messageView.html',
            controller: 'MessageViewCtrl'
          }
        }
      })


      .state('app.message-compose', {
        url: '/messages/compose',
        views: {
          'menuContent': {
            templateUrl: 'templates/messages/messageCompose.html',
            controller: 'MessageComposeCtrl'
          }
        }
      })

      .state('app.project', {
        url: '/project',
        views: {
          'menuContent': {
            templateUrl: 'templates/project/project.html',
            controller: 'ProjectCtrl'
          }
        }
      })

      .state('splash', {
        url: '/splash',
        templateUrl: 'templates/splash.html',
        controller: 'SplashCtrl'
      })

    .state('login', {
      url: '/login',
      templateUrl: 'templates/auth/login.html',
      controller: 'LoginCtrl'
    });


    // if none of the above states are matched, use this as the fallback
    //$urlRouterProvider.otherwise('/app/main');
    $urlRouterProvider.otherwise('/splash');
  });
