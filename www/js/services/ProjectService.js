var local = "http://local.gigasage.com:203";
var prod = "http://scienteer.com";

var server = local;

angular.module('scienteer.ProjectService', [])

  .service('ProjectService', function($q) {
    return {
      getProject: function(http) {
        var deferred = $q.defer();
        var promise = deferred.promise;

        var token = "Bearer: " + localStorage.getItem("token");

        var url = server + "/api/v1/app/status/student";

        http({
          method : "GET",
          url : url,
          dataType:"jsonp",
          headers: {'Authorization': token,
            'Content-Type':'application/json'}
        }).then(function success(response) {
          console.log("SUCCESS PROJECT CALL!");
          deferred.resolve(response);
        }, function error(response) {
          console.log("FAIL PROJECT CALL!");
          deferred.reject(status);
        });

        promise.success = function(fn) {
          promise.then(fn);
          return promise;
        }
        promise.error = function(fn) {
          promise.then(null, fn);
          return promise;
        }
        return promise;
      }
    }
  });
