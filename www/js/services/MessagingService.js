var local = "http://local.gigasage.com:203";
var prod = "http://scienteer.com";

var server = local;

angular.module('scienteer.MessagingService', [])

  .service('MessageService', function($q) {
    return {
      send: function(username, subject, body, http) {
        var deferred = $q.defer();
        var promise = deferred.promise;

        //Create AUTH body
        var obj = new Object();
        obj.to = username;
        obj.body  = body;
        obj.subject = subject;
        var auth= JSON.stringify(obj);

        var token = "Bearer: " + localStorage.getItem("token");

        var url = server + "/api/v1/app/messages/send";
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', token);

        var response = http.post(url, auth, {
          headers: {'Authorization': token,
            'Content-Type':'application/json'}
        }).success(function(data, status, headers, config){
          console.log("MessageService.Send Success");
          deferred.resolve(data);
        }).error(function(data, status, headers, config){
          console.log("MessageService.Send Fail");
          deferred.reject(status);
        });

        promise.success = function(fn) {
          promise.then(fn);
          return promise;
        }
        promise.error = function(fn) {
          promise.then(null, fn);
          return promise;
        }
        return promise;
      },

      delete: function(http, id) {
        var deferred = $q.defer();
        var promise = deferred.promise;

        var token = "Bearer: " + localStorage.getItem("token");

        var url = server + "/api/v1/app/message/" + id + "/delete" ;

        http({
          method : "DELETE",
          url : url,
          dataType:"jsonp",
          headers: {'Authorization': token,
            'Content-Type':'application/json'}
        }).then(function success(response) {
          console.log("SUCCESS TIMELINE CALL!");
          deferred.resolve(response);
        }, function error(response) {
          console.log("FAIL TIMELINE CALL!");
          deferred.reject(status);
        });

        promise.success = function(fn) {
          promise.then(fn);
          return promise;
        }
        promise.error = function(fn) {
          promise.then(null, fn);
          return promise;
        }
        return promise;
      },

      getMessage: function(http, id) {
        var deferred = $q.defer();
        var promise = deferred.promise;

        var token = "Bearer: " + localStorage.getItem("token");

        var url = server + "/api/v1/app/message/" + id ;

        http({
          method : "GET",
          url : url,
          dataType:"jsonp",
          headers: {'Authorization': token,
            'Content-Type':'application/json'}
        }).then(function success(response) {
          console.log("SUCCESS TIMELINE CALL!");
          deferred.resolve(response);
        }, function error(response) {
          console.log("FAIL TIMELINE CALL!");
          deferred.reject(status);
        });

        promise.success = function(fn) {
          promise.then(fn);
          return promise;
        }
        promise.error = function(fn) {
          promise.then(null, fn);
          return promise;
        }
        return promise;
      },

      getMessageList: function(http) {
        var deferred = $q.defer();
        var promise = deferred.promise;

        var token = "Bearer: " + localStorage.getItem("token");

        var url = server + "/api/v1/app/messages";

        http({
          method : "GET",
          url : url,
          dataType:"jsonp",
          headers: {'Authorization': token,
            'Content-Type':'application/json'}
        }).then(function success(response) {
          console.log("SUCCESS TIMELINE CALL!");
          deferred.resolve(response);
        }, function error(response) {
          console.log("FAIL TIMELINE CALL!");
          deferred.reject(status);
        });

        promise.success = function(fn) {
          promise.then(fn);
          return promise;
        }
        promise.error = function(fn) {
          promise.then(null, fn);
          return promise;
        }
        return promise;
      }
    }
  });
