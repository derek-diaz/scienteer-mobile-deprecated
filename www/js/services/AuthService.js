var local = "http://local.gigasage.com:203";
var prod = "http://scienteer.com";

var server = local;

angular.module('scienteer.AuthService', [])

  .service('LoginService', function($q) {
    return {
      loginUser: function(username, password, http) {
        var deferred = $q.defer();
        var promise = deferred.promise;

        //Create AUTH body
        var obj = new Object();
        obj.email_username = username;
        obj.password  = password;
        var auth= JSON.stringify(obj);

        var url = server + "/api/v1/app/auth/login";
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        var response = http.post(url, auth, {
          headers: headers
        }).success(function(data, status, headers, config){
          console.log("SUCCESS AUTH CALL!");
          deferred.resolve(data);
        }).error(function(data, status, headers, config){
          console.log("FAIL AUTH CALL!");
          deferred.reject(status);
        });

        promise.success = function(fn) {
          promise.then(fn);
          return promise;
        }
        promise.error = function(fn) {
          promise.then(null, fn);
          return promise;
        }
        return promise;
      }
    }
  });
