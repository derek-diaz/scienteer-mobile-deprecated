angular.module('scienteer.MessagingController', [])

  .controller('MessagesCtrl', function($scope, $ionicModal, $http, $state, MessageService) {
    $scope.messages = [];

    MessageService.getMessageList($http).success(function(data) {
      $scope.messages =  data.data.messages;
      console.log(data);
    }).error(function(data) {
      console.log(data.status);
    });

    $scope.doRefresh = function() {
      MessageService.getMessageList($http).success(function(data) {
        $scope.messages =  data.data.messages;
        console.log(data);
      }).error(function(data) {
        console.log(data.status);
      }) .finally(function() {
        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      });
    };

    $scope.remove = function(message) {
      MessageService.delete($http, message.id).success(function(data) {
        console.log(data);
        MessageService.getMessageList($http).success(function(data) {
          $scope.messages =  data.data.messages;
          console.log(data);
        }).error(function(data) {
          console.log(data.status);
        }) .finally(function() {
          // Stop the ion-refresher from spinning
          $scope.$broadcast('scroll.refreshComplete');
        });
      }).error(function(data) {
        console.log(data.status);
      });
    };

    $scope.compose = function(message) {
      $state.go('app.message-compose');
    };

  })

  .controller('MessageComposeCtrl', function($scope, $ionicModal,$ionicPopup,$state, $http, $stateParams, MessageService) {
    $scope.data = [];


    $scope.send = function() {

      if (!$scope.data.username || !$scope.data.body){
        var alertPopup = $ionicPopup.alert({
          title: 'Missing Information!',
          template: 'Make sure you include the username or email of the user and your message.'
        });
      }else{//Message and To is not empty
        MessageService.send($scope.data.username, $scope.data.subject, $scope.data.body, $http).success(function(data) {
          if(data.message == "success"){
            var alertPopup = $ionicPopup.alert({
              title: 'Message Sent!',
              template: 'Your message was sent successfully.'
            });
            $state.go('app.messages');
          }
          console.log(data);
        }).error(function(data, details, error) {
          console.log(data);
          var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: details
          });
        });
      }
    }



    ;
  })

  .controller('MessageViewCtrl', function($scope, $ionicModal, $http, $stateParams, MessageService) {
    $scope.message = "";
    $scope.subject = "";
    $scope.timestamp =  "";
    $scope.senderName =  "";
    $scope.senderPhoto =  "";

    MessageService.getMessage($http,$stateParams.messageId).success(function(data) {
      $scope.message =  data.data.message.body;
      $scope.subject =  data.data.message.subject;
      $scope.timestamp =  data.data.message.timestamp.date;
      $scope.senderName =  data.data.message.senderName;
      $scope.senderPhoto =  "http://local.gigasage.com:203" + data.data.message.senderPhoto;
      console.log(data);
    }).error(function(data) {
      console.log(data.status);
    });
  });
