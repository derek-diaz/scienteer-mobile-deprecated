angular.module('scienteer.AuthController', [])
  .controller('LoginCtrl', function($scope, LoginService, $ionicPopup, $state, $http, MultiLoading) {
    $scope.data = {};

    //Check if user is already logged in
    if(localStorage.getItem("token") !== null && localStorage.getItem("token") !== ""){
      $state.go('app.main');
      console.log("Login Found!");
      console.log(localStorage.getItem("token"));
    }

    //Login button is tapped
    $scope.login = function() {
      MultiLoading.show();
      LoginService.loginUser($scope.data.username, $scope.data.password, $http).success(function(data) {

        localStorage.setItem("token", data.token);
        localStorage.setItem("profilePhoto", data.profilePhoto);
        localStorage.setItem("isPremium", data.premium);
        localStorage.setItem("name", data.name);
        localStorage.setItem("role", data.userTypes[Object.keys(data.userTypes)[0]]);

        console.log("New Token: " + localStorage.getItem("token"));
        console.log("New Profile: " + localStorage.getItem("profilePhoto"));
        console.log("New Premium: " + localStorage.getItem("isPremium"));
        console.log("New Role: " + localStorage.getItem("role"));
        console.log("New Name: " + localStorage.getItem("name"));

        $state.go('app.main');
      }).error(function(data) {
        MultiLoading.hide();
        var alertPopup = $ionicPopup.alert({
          title: 'Login failed!',
          template: 'Please check your credentials!'
        });
      });
    }

  })

  .controller('SplashCtrl', function($scope, $timeout, $state, $ionicViewSwitcher) {
    //Check if user is not logged in
    if(!localStorage.getItem("token") == null || !localStorage.getItem("token") == ""){
      setTimeout(function(){
        $ionicViewSwitcher.nextDirection('forward');
        $state.go('app.main');
        //$state.go('app.project');
      }, 500);
    }else{
      setTimeout(function(){
        $state.go('login');
        console.log("Login Not Found!");
      }, 1500);
    }
  });
