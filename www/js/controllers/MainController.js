angular.module('scienteer.MainController', [])
  .controller('MainCtrl', function($scope, $ionicModal, $ionicHistory,$window, $timeout, $state) {
    $scope.logout = function() {
      localStorage.setItem("token", "");
      localStorage.setItem("profilePhoto", "");
      localStorage.setItem("isPremium", "");
      localStorage.setItem("name", "");
      localStorage.setItem("role", "");
      $ionicHistory.clearCache();
      $window.location.reload(true);
      console.log("Logged out!");
      $state.go('login');
    };

    $scope.profilePhoto = 'http://local.gigasage.com:203' + localStorage.getItem("profilePhoto");
    $scope.name = localStorage.getItem("name");

    if (localStorage.getItem("role") == "fairadmin" ) {
      $scope.role = "Fair Administrator";
    } else if (localStorage.getItem("role") == "student"){
      $scope.role = "Student";
    }else {
      $scope.role = "NULL";
    }
  })

  .controller('MainMenuCtrl', function($scope, TimelineService, $http, MultiLoading) {
    MultiLoading.show();
    //Check if user is not logged in
    if(localStorage.getItem("token") == null && localStorage.getItem("token") == ""){
      $state.go('login');
      console.log("Login Not Found!");
    }

    $scope.name = localStorage.getItem("name");
    $scope.timeline = [];

    TimelineService.getTimeline($http).success(function(data) {
      $scope.timeline =  data.data.timeline;
      console.log(data);
    }).error(function(data) {
      console.log(data.status);
    });
    MultiLoading.hide();
  })


  .factory('MultiLoading', function($ionicLoading) {
    //Loading indicator
    var loading = {
      opened: 0,
      show: function() {
        if (loading.opened > 0)
          return;
        $ionicLoading.show({
          noBackdrop :false,
          template: '<ion-spinner icon="ripple"></ion-spinner>',
          duration :20000
        });
        loading.opened+=1;
      },
      hide: function() {
        loading.opened = 0;
        $ionicLoading.hide();
      }
    };
    return loading;
  });
