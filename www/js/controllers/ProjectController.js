angular.module('scienteer.ProjectController', [])

  .controller('ProjectCtrl', function($scope, $ionicModal, $http, ProjectService) {

    //Check if user is not logged in
    if(localStorage.getItem("token") == null && localStorage.getItem("token") == ""){
      $state.go('login');
      console.log("Login Not Found!");
    }

    $scope.accountPercent = "";
    $scope.accountText = "";
    $scope.accountColor =  "";
    $scope.projectPercent =  "";
    $scope.projectText =  "";
    $scope.projectColor =  "";

    ProjectService.getProject($http).success(function(data) {
      $scope.accountPercent = data.data.accountStatus.percent;
      $scope.accountText = data.data.accountStatus.appText;
      $scope.accountColor = "bg-" + data.data.accountStatus.color;
      $scope.projectPercent =  data.data.projectStatus.percent;
      $scope.projectText =  data.data.projectStatus.appText;
      $scope.projectColor =  "bg-" + data.data.projectStatus.color;
      console.log(data);
    }).error(function(data) {
      console.log(data.status);
    });

    $scope.doRefresh = function() {
      ProjectService.getProject($http).success(function(data) {
        $scope.accountPercent = data.data.accountStatus.percent;
        $scope.accountText = data.data.accountStatus.appText;
        $scope.accountColor = "bg-" + data.data.accountStatus.color;
        $scope.projectPercent =  data.data.projectStatus.percent;
        $scope.projectText =  data.data.projectStatus.appText;
        $scope.projectColor = "bg-" + data.data.projectStatus.color;
        console.log(data);
      }).error(function(data) {
        console.log(data.status);
      }).finally(function() {
        // Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      });
    };
  });
